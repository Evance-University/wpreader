angular.module('wpreader').config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'menu/menu.html'
    })

    .state('app.overview', {
      url: '/overview',
      views: {
        'menuContent': {
          templateUrl: 'overview/overview.html',
          controller: 'overviewController',
          controllerAs: 'vm'
        }
      }
    })

  .state('app.detail', {
    url: '/overview/:postId',
    views: {
      'menuContent': {
        templateUrl: 'detail/detail.html',
        controller: 'detailController',
        controllerAs: 'vm'
      }
    }
  });

  $urlRouterProvider.otherwise('/app/overview');
});
